﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class OptionsMenuController : MonoBehaviour
{

    [SerializeField]
    private TextMeshProUGUI textSave, textCancel, textTitle, textLabelToggleMusic, textLabelVibration;
    [SerializeField]
    private Toggle toggleMusic, toggleEffects, toggleVibration;

    private GameController gameController;


    public event Action OnCloseMenu = delegate { };

    private void Awake()
    {
        gameController = GameController.Instance;

    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnEnable()
    {
        toggleVibration.isOn = gameController.Vibration;

        //TODO ASIGNAR VALORES DE SONIDO

        toggleEffects.isOn = gameController.Vibration;
        toggleMusic.isOn = gameController.Vibration;
    }
    public void ButtonCancel()
    {
        OnCloseMenu();
    }

    public void ButtonSave()
    {
        //TODO: SAVE VALUES
        gameController.Vibration = toggleVibration.isOn;
        //TODO ASIGNAR VALORES DE SONIDO
        OnCloseMenu();
    }
}
