﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasController : MonoBehaviour {

    [SerializeField]
    private GameObject optionsMenu, gearbutton;


    // Start is called before the first frame update
    void Start()

    {
        optionsMenu.GetComponent<OptionsMenuController>().OnCloseMenu += HideMenuOptions;
    }


    private void OnDestroy()
    {
        if (optionsMenu)
        {
            optionsMenu.GetComponent<OptionsMenuController>().OnCloseMenu -= HideMenuOptions;

        }
    }
    // Update is called once per frame
    void Update()
    {

    }

    public void ShowMenuOptions()
    {
        optionsMenu.SetActive(true);
        gearbutton.SetActive(false);
    }

    public void HideMenuOptions()
    {
        optionsMenu.SetActive(false);
        gearbutton.SetActive(true);
    }
}
