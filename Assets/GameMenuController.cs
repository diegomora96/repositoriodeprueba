﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMenuController : MonoBehaviour
{
    [SerializeField]
    private GameObject startMenuPrefab, mapMenuPrefab;

    private Animator animator;
    private CanvasGroup canvasGroup;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        canvasGroup = GetComponent<CanvasGroup>();
        animator.SetBool("Shown", true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SeeMap()
    {
        UtilsCanvas.ChangeCanvas(mapMenuPrefab, gameObject);
        Hide();
    }

    public void SelectChallenge()
    {

    }

    private void Hide()
    {
        animator.SetBool("Shown", false);

        canvasGroup.interactable = false;
    }
}
