﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceGarbageController : EnemyBaseController {

    private Rigidbody body;

    private float DeadTime;

    [SerializeField]
    private SpaceGarbage spaceGarbage;

    public SpaceGarbage SpaceGarbage { get => spaceGarbage; set => spaceGarbage = value; }

    public event Action<SpaceGarbageController> OnDesactivate = delegate { };

    void Start()
    {
        DeadTime = 0;
        body = GetComponent<Rigidbody>();
        timerToAppear = Instantiate(timerPrefab);
        timerToAppear.SetActive(false);
        timerToAppear.GetComponent<Timer>().OnTimeDone += OnCanAppear;
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log("Live " + live + " " + "Active " + isActiveAndEnabled);
        if (gameObject.activeInHierarchy)
        {
            Move();
        }
    }


    public void Move()
    {
        body.MovePosition(body.position + Vector3.left * Time.deltaTime * spaceGarbage.speed);
    }


    public override void Reset()
    {
        gameObject.SetActive(true);
    }

    public void Deactivate()
    {
        timerToAppear.GetComponent<Timer>().WaitTime = spaceGarbage.waitTimeToReset;
        timerToAppear.SetActive(true);
        timerToAppear.GetComponent<Timer>().Reset();
        gameObject.SetActive(false);
    }
}
