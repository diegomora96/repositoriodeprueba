﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShipController : EnemyBaseController {
    private float deadTime;

    private Rigidbody body;

    [SerializeField]
    private Enemy enemy;

    [SerializeField]
    private GameObject ShotPoint;

    private List<LaserController> poolRedLaser;

    private bool canShoot;

    private GameObject timerToShoot;

    public Enemy Enemy { get => enemy; set => enemy = value; }

    public event Action<EnemyShipController> OnDesactivate = delegate { };
    // Start is called before the first frame update

    //Esto es otro comentario

    private void Awake()
    {
        //timerToAppear = Instantiate(timerPrefab);
        timerToShoot = Instantiate(timerPrefab);
        timerToShoot.name = "TimerToShoot";
        poolRedLaser = new List<LaserController>();

        for (int i = 0; i < enemy.maxSizePoolLasers; i++)
        {
            LaserController lasers = Instantiate(enemy.laserPrefab).GetComponent<LaserController>();
            lasers.gameObject.SetActive(false);
            poolRedLaser.Add(lasers);
        }
    }
    void Start()
    {
        deadTime = 0;
        body = GetComponent<Rigidbody>();

        //timerToAppear.SetActive(false);
        //timerToAppear.GetComponent<Timer>().OnTimeDone += OnCanAppear;

        timerToShoot.GetComponent<Timer>().WaitTime = enemy.waitTimeToShoot;
        timerToShoot.GetComponent<Timer>().OnTimeDone += OnCanShoot;
        timerToShoot.GetComponent<Timer>().Reset();


        for (int i = 0; i < poolRedLaser.Count; i++)
        {
            poolRedLaser[i].gameObject.SetActive(false);
        }
    }

    private void OnDestroy()
    {
        if (timerToAppear != null)
            timerToAppear.GetComponent<Timer>().OnTimeDone -= OnCanAppear;

        if (timerToShoot != null)
            timerToShoot.GetComponent<Timer>().OnTimeDone -= OnCanShoot;

    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log("Live " + live + " " + "Active " + isActiveAndEnabled);
        if (gameObject.activeInHierarchy)
        {
            Move();
            if (canShoot)
            {
                Shoot();
            }
        }
    }

    private void Shoot()
    {
        LaserController laserInactive = SearchLaser();
        if (laserInactive != null)
        {
            laserInactive.Speed = enemy.speedLaser;
            laserInactive.gameObject.SetActive(true);
            laserInactive.transform.position = ShotPoint.transform.position;
            canShoot = false;
            timerToShoot.SetActive(true);
            timerToShoot.GetComponent<Timer>().Reset();
        }
    }


    private LaserController SearchLaser()
    {
        LaserController laserInactive = poolRedLaser.Find(x => !x.isActiveAndEnabled);

        return laserInactive;
    }

    private void Move()
    {
        body.MovePosition(body.position + Vector3.left * Time.deltaTime * enemy.speed);
    }

    public void Deactivate()
    {
        OnDesactivate(this);
        timerToShoot.SetActive(false);

        gameObject.SetActive(false);
    }

    public void OnCanShoot()
    {
        canShoot = true;
    }

    public override void Reset()
    {
        gameObject.SetActive(true);
        timerToShoot.SetActive(true);
        timerToShoot.GetComponent<Timer>().Reset();
    }
    private void OnParticleCollision(GameObject other)
    {
        Debug.Log("Collision");
        Deactivate();
        //gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        //timerToShoot.SetActive(false);
    }

    private void OnEnable()
    {
        //Debug.Log("ON Enable");
        timerToShoot.SetActive(true);

        //gameObject.SetActive(true);

    }
}
