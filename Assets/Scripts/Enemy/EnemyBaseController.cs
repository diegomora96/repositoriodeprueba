﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EnemyBaseController : MonoBehaviour {

    [SerializeField]
    protected GameObject timerPrefab;

    protected GameObject timerToAppear;
    protected bool CanReset { get; set; } = true;

    public event Action<GameObject> OnCanReset = delegate { };
    public void OnCanAppear()
    {
        OnCanReset(this.gameObject);
    }
    public abstract void Reset();
    //{
    //    gameObject.SetActive(true);
    //}

    void Move()
    {

    }



}
