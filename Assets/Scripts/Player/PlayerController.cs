﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    private float speedBoost = 1.0f;
    BaseInputHandler controller;

    [SerializeField]
    private PlayerShip player;
    [SerializeField]
    private GameObject timerPrefab, shotPlace;

    private GameObject timerShootNeutralized, timerShootExpansiveWave, timerDamage;

    public bool CanMove { get; set; } = true;
    public bool CanBeDamaged { get; set; } = true;
    public bool CanShootNeutralizer { get; set; } = true;
    public bool CanShootExpansive { get; set; } = true;
    public PlayerShip Player { get => player; }

    public event Action<float> OnPlayerMove = delegate { };
    public event Action OnCanShootExpansiveWave = delegate { };
    public event Action OnCanShootNeutralizer = delegate { };
    public event Action OnGetDamage = delegate { };

    List<GameObject> expansiveWavePool;
    List<GameObject> newutralizerRayPool;

    private void Awake()
    {

        timerShootNeutralized = Instantiate(timerPrefab);
        timerShootNeutralized.SetActive(false);
        timerShootNeutralized.GetComponent<Timer>().OnTimeDone += OnPlayerCanShootNeutralizer;

        timerShootExpansiveWave = Instantiate(timerPrefab);
        timerShootExpansiveWave.SetActive(false);
        timerShootExpansiveWave.GetComponent<Timer>().OnTimeDone += OnPlayerCanShootExpansiveWave;

        timerDamage = Instantiate(timerPrefab);
        timerDamage.SetActive(false);
        timerDamage.GetComponent<Timer>().OnTimeDone += OnCanBeDamaged;
    }

    private void OnDestroy()
    {
        if (timerShootNeutralized != null)
        {
            timerShootNeutralized.GetComponent<Timer>().OnTimeDone -= OnPlayerCanShootNeutralizer;
        }
        if (timerShootExpansiveWave != null)
        {
            timerShootExpansiveWave.GetComponent<Timer>().OnTimeDone -= OnPlayerCanShootExpansiveWave;
        }
        if (timerDamage != null)
        {
            timerDamage.GetComponent<Timer>().OnTimeDone -= OnCanBeDamaged;
        }
    }

    #region Action Events
    private void OnCanBeDamaged()
    {
        //Debug.Log("Can Be Damaged at " + Time.deltaTime);
        CanBeDamaged = true;

        //Stop Make player intermintent
    }

    private void OnPlayerCanShootNeutralizer()
    {
        OnCanShootNeutralizer();
        CanShootNeutralizer = true;
    }

    private void OnPlayerCanShootExpansiveWave()
    {
        OnCanShootExpansiveWave();
        CanShootExpansive = true;
    }
    #endregion


    void Start()
    {
        controller = GameObject.FindGameObjectWithTag("Canvas").GetComponent<CanvasInputController>();
        //#if UNITY_EDITOR
        //        controller = GetComponent<KeyboardInputHandler>();
        //#endif
        expansiveWavePool = new List<GameObject>();
        newutralizerRayPool = new List<GameObject>();

        //Generacion Pool Expansive Waves
        for (int i = 0; i < 3; i++)
        {
            GameObject expansiveWavePrefab = player.ExpansiveWave.prefab;
            expansiveWavePrefab.SetActive(false);
            expansiveWavePool.Add(Instantiate(expansiveWavePrefab));
        }
        //Generacion Pool Neutralizer Rays
        for (int i = 0; i < 3; i++)
        {
            GameObject neutralizerRayPrefab = player.NeutralizerRay.prefab;
            neutralizerRayPrefab.SetActive(false);
            newutralizerRayPool.Add(Instantiate(neutralizerRayPrefab));
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (CanMove)
        {
            Move();
        }
    }


    internal GameObject GetExpansiveWave()
    {
        GameObject expansiveWave = null;
        for (int i = 0; i < expansiveWavePool.Count; i++)
        {
            if (!expansiveWavePool[i].activeInHierarchy)
            {
                expansiveWave = expansiveWavePool[i];
            }
        }
        return expansiveWave;
    }

    internal GameObject GetNeutralizerRay()
    {
        GameObject neutralizerRay = null;
        for (int i = 0; i < newutralizerRayPool.Count; i++)
        {
            if (!newutralizerRayPool[i].activeInHierarchy)
            {
                neutralizerRay = newutralizerRayPool[i];
            }
        }
        return neutralizerRay;
    }
    internal void ShootNeutralizer()
    {
        //Debug.Log("Actual Time " + Time.fixedTime + " Last Time " + lastTimeShooted + " NEXT SHOT " + (lastTimeShooted + shotCadence) + " Cadence " + Constants.INITIAL_CADENCE);
        GameObject neutralizer = GetNeutralizerRay();
        if (CanShootNeutralizer && neutralizer)
        {
            timerShootNeutralized.GetComponent<Timer>().WaitTime = player.WaitTimeNeutralizer;
            timerShootNeutralized.SetActive(true);
            timerShootNeutralized.GetComponent<Timer>().Reset();
            CanShootNeutralizer = false;

            InstantiateAbility(neutralizer);
        }
    }


    internal void ShootExpansiveWave()
    {
        //Debug.Log("Actual Time " + Time.fixedTime + " Last Time " + lastTimeShooted + " NEXT SHOT " + (lastTimeShooted + shotCadence) + " Cadence " + Constants.INITIAL_CADENCE);

        GameObject expansiveWave = GetExpansiveWave();
        if (CanShootExpansive && expansiveWave)
        {
            timerShootExpansiveWave.GetComponent<Timer>().WaitTime = player.WaitTimeExpansiveWave;
            timerShootExpansiveWave.SetActive(true);
            timerShootExpansiveWave.GetComponent<Timer>().Reset();
            CanShootExpansive = false;

            InstantiateAbility(expansiveWave);
        }
    }

    private void InstantiateAbility(GameObject prefab)
    {
        prefab.transform.position = shotPlace.transform.position;
        prefab.SetActive(true);
    }

    private void Move()
    {
        float movHorizontal = controller.Horizontal;
        float movVertical = controller.Vertical;

        if (movHorizontal != 0.1f || movVertical >= 0.1f)
        {
            transform.Translate(Vector3.right * movHorizontal * player.Speed * speedBoost * Time.deltaTime);
            OnPlayerMove(Vector3.right.x * movHorizontal * player.Speed * speedBoost * Time.deltaTime);
            transform.Translate(Vector3.up * movVertical * player.Speed * speedBoost * Time.deltaTime);
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        PowerUpController powerUpController = other.gameObject.GetComponent<PowerUpController>();
        EnemyShipController enemyController = other.gameObject.GetComponent<EnemyShipController>();
        LaserController laserController = other.gameObject.GetComponent<LaserController>();
        SpaceGarbageController spacegarbage = other.gameObject.GetComponent<SpaceGarbageController>();

        
        if (powerUpController != null)
        {
            PowerUpController powerUp;

        }
        else if (enemyController != null || laserController != null || spacegarbage != null)
        {
            if (CanBeDamaged)
            {
                CanBeDamaged = !CanBeDamaged;
                OnGetDamage();

                timerDamage.GetComponent<Timer>().WaitTime = player.WaitTimeUntilNextHit;
                timerDamage.SetActive(true);
                timerDamage.GetComponent<Timer>().Reset();
                //Debug.Log("Damaged at " + Time.fixedTime);
            }

            if (enemyController)
            {
                enemyController.Deactivate();
            }
            if (laserController)
            {
                laserController.Deactivate();
            }
            if (spacegarbage)
            {
                spacegarbage.Deactivate();
            }
        }
    }
}
