﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CanvasInputController : BaseInputHandler, IPointerUpHandler, IPointerDownHandler {

    [SerializeField]
    private Button neutralizerButton, expansiveWaveButton;

    private float horizontal, horizontalSpeed, horizontalTarget;
    private float vertical, verticalSpeed, verticalTarget;

    private readonly float smoothTime = 0.1f;
    private PlayerController playerController;

    [SerializeField]
    private Joystick joystick;
    private Vector3 direction;

    public override float Horizontal {
        get {
            return horizontal;
        }
    }

    public override float Vertical {
        get {
            return vertical;
        }
    }
    public override Vector3 Direction {
        get {
            return direction;
        }
    }

    protected bool pressed;
    private void Start()
    {
        //TODO: Pensar refactorizacion para evitar buscar por Tag
        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();

    }

    // Update is called once per frame
    void Update()
    {
        GetMoves();
    }

    public void ChangeHorizontalAxis(float horizontalValue)
    {
        horizontalTarget = horizontalValue;
    }

    public void ChangeVerticalAxis(float verticalValue)
    {
        verticalTarget = verticalValue;
    }

    public void ShootNeutralizer()
    {
        neutralizerButton.interactable = false;
        playerController.ShootNeutralizer();
    }

    public void ThrowExpansiveWaveShoot()
    {
        expansiveWaveButton.interactable = false;
        playerController.ShootExpansiveWave();
    }

    internal override void GetMoves()
    {
        //Forma antigua con flechas
        //horizontal = Mathf.SmoothDamp(horizontal, horizontalTarget, ref horizontalSpeed, smoothTime);
        //vertical = Mathf.SmoothDamp(vertical, verticalTarget, ref verticalSpeed, smoothTime);

        horizontal = joystick.Horizontal;
        vertical = joystick.Vertical;
        //direction = Vector3.forward * joystick.Vertical + Vector3.right * joystick.Horizontal * Time.fixedDeltaTime;

        //horizontal = Input.GetAxis("Horizontal");
        //vertical = Input.GetAxis("Vertical");
        //vertical = joystick.Vertical;

    }

    public void OnPointerUp(PointerEventData eventData)
    {
        pressed = false;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        pressed = true;
    }
}
