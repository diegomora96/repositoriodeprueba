﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardInputHandler : BaseInputHandler {
    float horizontal, vertical;
    Vector3 direction;
    PlayerController playerController;
    public override float Horizontal {
        get {
            return horizontal;
        }
    }

    public override float Vertical {
        get {
            return vertical;
        }
    }

    public override Vector3 Direction {
        get {
            return direction;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        playerController = GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        GetMoves();
        GetInputs();
    }
    internal override void GetMoves()
    {
        horizontal = Input.GetAxis("Horizontal");
        vertical = Input.GetAxis("Vertical");
    }
    internal void GetInputs()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            //ShootNeutralizer();
        }
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            //ThrowExpansiveWaveShoot();
        }
    }

    private void ShootNeutralizer()
    {
        playerController.ShootNeutralizer();
    }

    private void ThrowExpansiveWaveShoot()
    {
        playerController.ShootExpansiveWave();
    }
}
