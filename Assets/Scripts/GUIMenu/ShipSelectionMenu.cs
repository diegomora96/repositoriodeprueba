﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ShipSelectionMenu : MonoBehaviour {
    [SerializeField]
    private GameObject startMenuPrefab, mapMenuPrefab, loadingPrefab, infoPlaceHolder, shipPlaceHolder, candado;

    [SerializeField]
    private TextMeshProUGUI textShipName, textLife, textSpeed, textNeutralizer, TextExpansive, textInvencible;

    private GameController gameController;

    private GameObject shipInstance, buttonInfo;

    private List<GameObject> avatarsInstances = new List<GameObject>();

    private List<PlayerShip> playerShips = new List<PlayerShip>();

    private GUIPlayerShip playerShipUIInfo;

    private Animator animator;
    private CanvasGroup canvasGroup;

    private bool shipShowing;

    private void Awake()
    {
        gameController = GameController.Instance;
    }
    // Start is called before the first frame update
    void Start()
    {
        shipShowing = true;
        playerShipUIInfo = gameController.PlayerShipUIInfo;
        playerShips = playerShipUIInfo.PlayerShips;
        LoadShips(playerShips);

        animator = GetComponent<Animator>();
        canvasGroup = GetComponent<CanvasGroup>();
        infoPlaceHolder.gameObject.SetActive(false);

        if (gameController.PlayerShipUIInfo.selectedShip)
        {
            ChangeShipSelected(playerShipUIInfo.selectedShip);
        }
        else
        {
            ChangeShipSelected(playerShipUIInfo.PlayerShips[0]);
        }
    }

    private void Update()
    {
        if (shipInstance)
        {
            shipInstance.transform.Rotate(Vector3.up * 3.0f);
        }
    }

    private void ChangeShipSelected(PlayerShip ship)
    {
        //soundManager.PlayMusicClick();
        playerShipUIInfo.selectedShip = ship;
        //if (gameController.GetAvatarsUnlocked().Contains(ship))
        //{
        //textContinue.GetComponent<Button>().interactable = true;
        //candado.SetActive(false);
        ChangeTitleName(ship);
        CreateTextInfoShip(ship);
        gameController.selectedPlayerShip = ship;
        if (shipInstance)
        {
            shipInstance.gameObject.SetActive(false);
        }
        shipInstance = avatarsInstances.Find(x => x.gameObject.name == ship.name);
        //shipInstance.gameObject.SetActive(true);

        if (shipShowing)
        {
            infoPlaceHolder.gameObject.SetActive(false);
            shipInstance.gameObject.SetActive(true);
        }
        else
        {
            infoPlaceHolder.gameObject.SetActive(true);
            shipInstance.gameObject.SetActive(false);
        }

        //}
        //else
        //{
        //    if (avatarInstance)
        //    {
        //        avatarInstance.gameObject.SetActive(false);
        //    }
        //    candado.SetActive(true);
        //    textContinue.GetComponent<Button>().interactable = false;
        //}
    }

    private void LoadShips(List<PlayerShip> shipsToShow)
    {

        //Instanciamos los gameobjects de los players
        for (int i = 0; i < playerShips.Count; i++)
        {
            PlayerShip item = playerShips[i];
            GameObject instanceAvatar = Instantiate(item.Prefab);
            instanceAvatar.gameObject.SetActive(false);
            instanceAvatar.transform.localScale = new Vector3(150.0f, 150.0f, 150.0f);

            instanceAvatar.GetComponent<PlayerController>().enabled = false;
            instanceAvatar.GetComponent<KeyboardInputHandler>().enabled = false;
            instanceAvatar.transform.SetParent(shipPlaceHolder.transform, false);
            instanceAvatar.name = playerShips[i].name;
           
            if (!avatarsInstances.Contains(instanceAvatar))
            {
                avatarsInstances.Add(instanceAvatar);
            }
        }
    }
    public void ClickRightArrow()
    {
        //soundManager.PlayMusicClick();
        int newId = playerShipUIInfo.selectedShip.IdShip + 1;
        if (newId >= playerShips.Count)
        {
            newId = 0;
        }
        PlayerShip newShip = playerShips[newId];
        ChangeShipSelected(newShip);
    }

    private void ChangeTitleName(PlayerShip playerShip)
    {
        textShipName.text = playerShip.ShipName;
    }

    public void ClickLeftArrow()
    {
        //soundManager.PlayMusicClick();
        int newId = playerShipUIInfo.selectedShip.IdShip - 1;
        if (newId < 0)
        {
            newId = playerShips.Count - 1;
        }
        PlayerShip newShip = playerShips[newId];
        ChangeShipSelected(newShip);
    }

    public void Continue()
    {
        Hide();
        UtilsCanvas.ChangeCanvas(loadingPrefab, gameObject);

    }

    public void Back()
    {

        //Debug.Log("continue");
        UtilsCanvas.ChangeCanvas(mapMenuPrefab, gameObject);

    }

    private void Hide()
    {
        animator.SetBool("Shown", false);

        canvasGroup.interactable = false;
    }

    public void ShowInfo()
    {
        SetPlaceHolderStatus();
        shipShowing = !shipShowing;
    }

    private void SetPlaceHolderStatus()
    {
        if (shipShowing)
        {
            infoPlaceHolder.gameObject.SetActive(true);
            shipInstance.gameObject.SetActive(false);
        }
        else
        {
            shipInstance.gameObject.SetActive(true);
            infoPlaceHolder.gameObject.SetActive(false);
        }
    }

    private void CreateTextInfoShip(PlayerShip ship)
    {

        SetInfoText(textLife, "Life", ship.Life.ToString());
        SetInfoText(textSpeed, "Speed", ship.Speed.ToString());
        SetInfoText(TextExpansive, "Cadence Expansive ", ship.WaitTimeExpansiveWave + "s");
        SetInfoText(textNeutralizer, "Cadence Neutralize", ship.WaitTimeNeutralizer + "s");
        SetInfoText(textInvencible, "Invencible time", ship.WaitTimeUntilNextHit + "s");
    }

    private void SetInfoText(TextMeshProUGUI newText, string title, string value)
    {
        newText.text = title + " " + value;
    }

}
