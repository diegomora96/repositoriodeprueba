﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class StartMenuController : MonoBehaviour
{
    [SerializeField]
    private GameObject mapMenuPrefab;
    [SerializeField]
    private TextMeshProUGUI startText, creditsText;

    private GameController gameController;

    private Animator animator;
    private CanvasGroup canvasGroup;

    private void Awake()
    {
        gameController = GameController.Instance;
    }

    // Start is called before the first frame update
    void Start()
    {
        gameController.Initialize();

        gameController.PauseGame(false);


        startText.text = "Start";
        creditsText.text = "Credits";
        animator = GetComponent<Animator>();
        canvasGroup = GetComponent<CanvasGroup>();
        animator.SetBool("Shown", true);
    }

    public void StartButton()
    {

        UtilsCanvas.ChangeCanvas(mapMenuPrefab, gameObject);
        Hide();
    }

    private void Hide()
    {
        animator.SetBool("Shown", false);

        canvasGroup.interactable = false;
    }

    public void CreditsButtons()
    {

    }
}
