﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapMenu : MonoBehaviour
{
    [SerializeField]
    private GameObject startMenuPrefab, ShipSelectionMenu;


    private GameController gameController;

    private Animator animator;
    private CanvasGroup canvasGroup;

    private void Awake()
    {
        gameController = GameController.Instance;
    }

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        canvasGroup = GetComponent<CanvasGroup>();
        animator.SetBool("Shown", true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void Hide()
    {
        animator.SetBool("Shown", false);

        canvasGroup.interactable = false;
    }
}
