﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;



public class LevelController : MonoBehaviour {
    [SerializeField]
    private Canvas canvas;
    [SerializeField]
    private TextMeshProUGUI TimeValueText;
    [SerializeField]
    private GameObject spawnersEnemyParent;
    [SerializeField]
    private KillZoneController killZoneController;

    [SerializeField]
    private HomePositionController homePositionController;

    [SerializeField]
    private GameObject initPosition;

    [SerializeField]
    private Goal goalPosition;

    [SerializeField]
    private PoolManager poolManager;

    PlayerController player;
    //Objects Intantiated USAR ESTOS
    private List<GameObject> enemiesList;
    private List<GameObject> garbageList;
    private int PlayerLifes;

    private GameObject timerUntilFirstEnemyInstantiated;
    private GameObject timerUntilFirstGarbageInstantiated;

    private GameController gameController;
    //Canvas
    CanvasButton canvasButton;
    CanvasInputController canvasInputController;
    private int enemiesDefeated;

    private void Awake()
    {
        gameController = GameController.Instance;
        if (gameController.PersistentData == null)
        {
            gameController.Initialize();
        }
        PlayerShip playerPrefab = null;

        if (gameController.selectedPlayerShip == null)
        {
            playerPrefab = Resources.Load<GameObject>("Prefabs/PlayerShips/NavePlayer").GetComponent<PlayerController>().Player;
        }
        else
        {
            playerPrefab = gameController.selectedPlayerShip;
        }

        GameObject playerInstantiated = GameObject.Instantiate(playerPrefab.Prefab);
        playerInstantiated.transform.localPosition = initPosition.transform.position;
        player = playerInstantiated.GetComponent<PlayerController>();
        killZoneController.playerController = player;

        enemiesList = new List<GameObject>();
        garbageList = new List<GameObject>();

        poolManager.LoadSpawners(spawnersEnemyParent);
    }



    // Start is called before the first frame update
    void Start()
    {
        canvasButton = canvas.GetComponent<CanvasButton>();
        canvasInputController = canvas.GetComponent<CanvasInputController>();
        gameController.PauseGame(false);
        PlayerLifes = player.Player.Life;

        timerUntilFirstEnemyInstantiated = poolManager.InstantiateTimer();
        timerUntilFirstEnemyInstantiated.name = "timerUntilFirstEnemyInstantiated";

        timerUntilFirstGarbageInstantiated = poolManager.InstantiateTimer();
        timerUntilFirstEnemyInstantiated.name = "timerUntilFirstEnemyInstantiated";

        //TODO: Quitar cuando se acaben las pruebas
        if (gameController.SelectedLevel == null)
        {
            gameController.SelectedLevel = gameController.LevelUIInfo.levels[0];
        }

        poolManager.EnemiesOfLevel = gameController.SelectedLevel.Enemies;
        poolManager.GarbageOfLevel = gameController.SelectedLevel.Obstacles;
        //Inicio Timers
        timerUntilFirstEnemyInstantiated.GetComponent<Timer>().WaitTime = gameController.SelectedLevel.TimeUntilFirstEnemy;
        timerUntilFirstEnemyInstantiated.GetComponent<Timer>().Reset();

        timerUntilFirstGarbageInstantiated.GetComponent<Timer>().WaitTime = gameController.SelectedLevel.TimeUntilFirstGarbage;
        timerUntilFirstGarbageInstantiated.GetComponent<Timer>().Reset();

        //Suscripcion a eventos
        timerUntilFirstEnemyInstantiated.GetComponent<Timer>().OnTimeDone += OnTimeToReleaseEnemies;
        timerUntilFirstGarbageInstantiated.GetComponent<Timer>().OnTimeDone += OnTimeToReleaseGarbage;

        canvasButton.OnPauseGame += OnPauseGame;
        canvasButton.OnExitLevel += OnExitLevel;
        canvasButton.OnRestartLevel += OnRestartlevel;
        canvasButton.OnGoHomeResponse += OnGoHomeResponse;

        player.OnPlayerMove += OnPlayerMoveSetSpawnerPos;
        player.OnPlayerMove += OnPlayerMoveKillZonePos;
        player.OnCanShootExpansiveWave += canvasButton.OnEnableExpansiveWaveButton;
        player.OnCanShootNeutralizer += canvasButton.OnEnableNeutralizerButton;
        player.OnGetDamage += OnPlayerDamage;

        homePositionController.OnGoHomePosition += OnGoHomePosition;
        goalPosition.OnGoalTrigger += OnGoalReached;

        canvasButton.OnChangeLife(player.Player.Life);
    }



    #region Events
    //TODO: Buscar utilidad a esto
    private void OnChangeLifePlayer(int life)
    {
        //poner sonidos
    }


    public void OnPlayerDamage()
    {
        PlayerLifes--;
        canvasButton.OnChangeLife(PlayerLifes);
        OnChangeLifePlayer(PlayerLifes);
        StartCoroutine(nameof(Spark));

        if (PlayerLifes <= 0)
        {
            PlayerDead();
        }
    }

    public void OnGoalReached()
    {
        ShowStats();
    }

    public void OnPauseGame(bool menuPauseState)
    {
        gameController.PauseGame(menuPauseState);
    }

    public void OnGoHomePosition()
    {
        canvasButton.ShowGoHomeMenu(true);
        gameController.PauseGame(true);
    }

    public void OnGoHomeResponse(bool status)
    {
        canvasButton.ShowGoHomeMenu(false);
        if (status)
        {
            Debug.Log("SALIMOS NIVEL");
            SceneManager.LoadScene(Scenes.SCENE_MENU_INICIAL);
            //salir del nivel
        }
        else
        {
            //Reset position
            player.transform.position = initPosition.transform.position;
            player.transform.rotation = initPosition.transform.rotation;
        }
        gameController.PauseGame(false);
    }
    #endregion


    private void OnDestroy()
    {
        if (canvasButton != null)
        {
            canvasButton.OnPauseGame -= OnPauseGame;
            canvasButton.OnGoHomeResponse -= OnGoHomeResponse;
            canvasButton.OnRestartLevel -= OnRestartlevel;
            canvasButton.OnGoHomeResponse -= OnGoHomeResponse;
        }
        if (player != null && canvasButton != null)
        {
            player.OnCanShootExpansiveWave -= canvasButton.OnEnableExpansiveWaveButton;
            player.OnCanShootExpansiveWave -= canvasButton.OnEnableNeutralizerButton;
            player.OnGetDamage -= OnPlayerDamage;
            player.OnPlayerMove -= OnPlayerMoveSetSpawnerPos;
        }
        if (timerUntilFirstEnemyInstantiated != null)
        {
            timerUntilFirstEnemyInstantiated.GetComponent<Timer>().OnTimeDone -= OnTimeToReleaseEnemies;
        }
        if (homePositionController != null)
        {
            homePositionController.OnGoHomePosition -= OnGoHomePosition;
        }
        if (timerUntilFirstGarbageInstantiated != null)
        {
            timerUntilFirstGarbageInstantiated.GetComponent<Timer>().OnTimeDone -= OnTimeToReleaseGarbage;
        }

        if (goalPosition != null)
        {
            goalPosition.OnGoalTrigger -= OnGoalReached;
        }
    }

    #region Spawners

    private void OnPlayerMoveSetSpawnerPos(float moveRight)
    {
        foreach (Transform item in spawnersEnemyParent.GetComponentInChildren<Transform>())
        {
            if (item.gameObject != spawnersEnemyParent.gameObject)
            {
                Spawner spawner = item.gameObject.GetComponent<Spawner>();
                if (player.transform.position.x > -18)
                {
                    spawner.Move(item.gameObject.transform.position.x + moveRight);
                }
            }
        }
    }

    private void OnPlayerMoveKillZonePos(float moveRight)
    {
        killZoneController.Move(killZoneController.gameObject.transform.position.x + moveRight);
    }



    #endregion

    // Update is called once per frame
    void Update()
    {
        float actualTime = Time.fixedTime;
        TimeValueText.text = string.Format("Time {0}", actualTime.ToString());
        //offset = Math.Abs(spawnerTest.gameObject.transform.position.x - player.gameObject.transform.position.x);
        //Debug.Log(offset);
        //Debug.Log("Active Enemies " + ActiveEnemies().Count);
        //Debug.Log("Spawners libres " + spawners.FindAll(x => x.CanUse).Count);
        //if(poolManager.ActiveElements(enemiesList).Count == 0)
        //{
        //    OnTimeToReleaseEnemies();
        //}
    }


    public void PlayerDead()
    {
        Debug.Log("Player Dead");
        ShowStats();
    }

    private void ShowStats()
    {
        player.CanMove = false;
        canvasButton.ShowStats(true);
        gameController.PauseGame(true);

        int enemiesDefeated = poolManager.EnemiesDefeated;
        float timeAtGoal = (float)(Math.Round((double)Time.timeSinceLevelLoad, 2));

        int score = GetScore(enemiesDefeated, timeAtGoal);
        canvasButton.SetValuesStats(score);
        //TODO Quitar cuando se acaben pruebas
        if (!gameController.SelectedLevel)
        {
            gameController.SelectedLevel = gameController.LevelUIInfo.levels[0];
        }


        gameController.PersistentData.AddScore(gameController.SelectedLevel, score);
        gameController.SaveInformation();
    }

    private int GetScore(int enemiesDefeated, float timeAtGoal)
    {

        int pointToSubstract = GetPointsToSubstract(enemiesDefeated);
        int points = GetPointsByTime(timeAtGoal);
        return points - pointToSubstract;
    }

    private static int GetPointsToSubstract(int enemiesDefeated)
    {
        int pointToSubstract;
        if (enemiesDefeated >= 0 && enemiesDefeated < 10)
        {
            pointToSubstract = 0;
        }
        else
        {
            pointToSubstract = 30;
        }

        return pointToSubstract;
    }

    private static int GetPointsByTime(float timeAtGoal)
    {
        int points = 0;

        if (timeAtGoal <= 10)
        {
            points = 500;
        }
        else if (timeAtGoal > 10 && timeAtGoal <= 20)
        {
            points = 400;

        }
        else if (timeAtGoal > 20 && timeAtGoal <= 100)
        {
            points = 300;

        }

        return points;
    }

    public void OnExitLevel()
    {
        SceneManager.LoadScene(Scenes.SCENE_MENU_INICIAL);

    }

    public void OnRestartlevel()
    {
        //Debug.Log("Restart");
        SceneManager.LoadScene(gameController.SelectedLevel.SceneName);
    }

    IEnumerator Spark()
    {
        while (!player.CanBeDamaged)
        {
            player.gameObject.SetActive(false);
            yield return new WaitForSeconds(0.1f);
            player.gameObject.SetActive(true);
            yield return new WaitForSeconds(0.1f);
        }
        yield return null;
    }

    private void OnTimeToReleaseEnemies()
    {
        for (int enemiesInScreen = 0; enemiesInScreen < gameController.SelectedLevel.MaxEnemiesInScreen; enemiesInScreen++)
        {
            Spawner randomSpawner = poolManager.GetRandomSpawner();
            poolManager.DeactivateTemporallySpawner(randomSpawner);

            if (poolManager.ActiveElements(enemiesList).Count < gameController.SelectedLevel.MaxEnemiesInScreen && randomSpawner)
            {
                GameObject enemyInstance = poolManager.InstantiateEnemy(randomSpawner);
                enemiesList.Add(enemyInstance);
            }

        }
    }

    private void OnTimeToReleaseGarbage()
    {
        for (int i = 0; i < poolManager.GarbageOfLevel.Count; i++)
        {
            Spawner randomSpawner = poolManager.GetRandomSpawner();
            poolManager.DeactivateTemporallySpawner(randomSpawner);

            if (poolManager.ActiveElements(garbageList).Count < gameController.SelectedLevel.MaxEnemiesInScreen && randomSpawner)
            {
                GameObject gargabePrefabInstantiated = poolManager.InstantiateGargabe(randomSpawner);
                garbageList.Add(gargabePrefabInstantiated);
            }
        }
    }
}
