﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasButton : MonoBehaviour {
    [SerializeField]
    private GameObject upArrow, downArrow, leftArrow, rightArrow;
    [SerializeField]
    private Button neutralizerButton, expansiveWaveButton, pauseButton;
    [SerializeField]
    private GameObject pauseMenu;
    [SerializeField]
    private GameObject statsMenu;
    [SerializeField]
    private GameObject goHomeMenu;

    [SerializeField]
    private Transform lifePlaceHolder;

    public event Action OnRestartLevel = delegate { };
    public event Action OnExitLevel = delegate { };
    public event Action<bool> OnPauseGame = delegate { };
    public event Action<bool> OnGoHomeResponse = delegate { };
    public event Action<int> OnChangeLifePlayer = delegate { };
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        //CheckEnabledButtons();
    }


    public void OnEnableExpansiveWaveButton()
    {
        expansiveWaveButton.interactable = true;
    }

    public void OnEnableNeutralizerButton()
    {
        neutralizerButton.interactable = true;
    }

    public void ShowButtons(bool state)
    {
        neutralizerButton.gameObject.SetActive(state);
        expansiveWaveButton.gameObject.SetActive(state);
        pauseButton.gameObject.SetActive(state);
        upArrow.gameObject.SetActive(state);
        downArrow.gameObject.SetActive(state);
        leftArrow.gameObject.SetActive(state);
        rightArrow.gameObject.SetActive(state);
        //TODO: Desactivar Joystick
    }

    public void ShowStats(bool state)
    {
        ShowButtons(false);
        statsMenu.gameObject.SetActive(state);
    }

    public void SetValuesStats(int score)
    {
        statsMenu.GetComponent<StatsMenuController>().SetValues(score);
    }

    public void ShowGoHomeMenu(bool state)
    {
        goHomeMenu.gameObject.SetActive(state);
    }

    public void ResponseGoHome(bool state)
    {
        OnGoHomeResponse(state);
    }

    #region UI Inputs

    public void PauseGame()
    {
        pauseMenu.gameObject.SetActive(true);
        ShowButtons(false);
        OnPauseGame(true);

    }

    public void ResumeGame()
    {
        pauseMenu.gameObject.SetActive(false);
        ShowButtons(true);
        OnPauseGame(false);
    }


    public void RestartLevel()
    {
        OnRestartLevel();
    }

    public void ExitLevel()
    {
        OnExitLevel();
    }


    public void OnChangeLife(int life)
    {
        DeleteChildrens();
        for (int i = 0; i < life; i++)
        {
            GameObject lifeItms = Instantiate(upArrow);
            lifeItms.SetActive(true);
            lifeItms.transform.SetParent(lifePlaceHolder);
        }
    }

    private void DeleteChildrens()
    {
        if (lifePlaceHolder.childCount > 0)
        {
            foreach (Transform child in lifePlaceHolder.GetComponentInChildren<Transform>())
            {
                if (child.gameObject != lifePlaceHolder.gameObject)
                {
                    child.gameObject.SetActive(false);
                }
            }
        }
    }
    #endregion
}
