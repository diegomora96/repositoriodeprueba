﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class StatsMenuController : MonoBehaviour {
    //[SerializeField]
    //private CanvasButton buttonReiniciar, buttonSalir;

    [SerializeField]
    private TextMeshProUGUI textPuntuacion, textTiempo;



    public void SetValues(int puntuacion)
    {
        textPuntuacion.text = String.Format("Score" + " {0}", puntuacion);
        textTiempo.text = String.Format("Time" + " {0}", Time.timeSinceLevelLoad.ToString());
    }
}
