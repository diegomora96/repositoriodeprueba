﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

//TODO: Clase para almacenar informacion del player durante el juego, sera la clase que serializaremos en un futuro

[Serializable]
public class PairLevelScore {
    [SerializeField]
    private String levelName;
    [SerializeField]
    private int score;

    public PairLevelScore(string levelName, int score)
    {
        this.levelName = levelName;
        this.score = score;
    }

    public string LevelName {
        get {
            return levelName;
        }

        set {
            levelName = value;
        }
    }

    public int Score {
        get {
            return score;
        }

        set {
            score = value;
        }
    }
}

[Serializable]
public class PersistentData {
    [SerializeField]
    private List<int> levelsUnlocked = new List<int>();
    [SerializeField]
    private List<int> shipsUnlocked = new List<int>();


    [SerializeField]
    private List<PairLevelScore> scores = new List<PairLevelScore>();



    public PersistentData()
    {
    }

    public List<int> LevelsUnlocked {
        get {
            return levelsUnlocked;
        }
    }

    public List<int> ShipsUnlocked {
        get {
            return shipsUnlocked;
        }
    }

    //TODO Añadir mejoras
    public void UnlockShip (PlayerShip playerShip)
    {
        if (!shipsUnlocked.Contains(playerShip.IdShip))
        {
            shipsUnlocked.Add(playerShip.IdShip);
        }
    }

    public void Unlocklevel(Level level)
    {
        if (!levelsUnlocked.Contains(level.IdLevel))
        {
            levelsUnlocked.Add(level.IdLevel);
        }
    }


    public List<PairLevelScore> Scores {
        get {
            return scores;
        }

        set {
            scores = value;
        }
    }

    public void AddScore(Level level, int score)
    {
        PairLevelScore pair = new PairLevelScore(level.LevelName1, score);
        if (scores.Any(x => x.LevelName == level.LevelName1))
        {
            int oldScore;
            PairLevelScore oldPair = scores.Single(x => x.LevelName == level.LevelName1);

            if (score > oldPair.Score)
            {
                scores.Remove(oldPair);
            }
        }
        scores.Add(pair);
    }

    public int GetScore(Level level)
    {
        int score = 0;
        if (scores.Any(x => x.LevelName == level.LevelName1))
        {
            PairLevelScore pair = scores.Single(x => x.LevelName == level.LevelName1);
            score = pair.Score;
        }
        return score;
    }


}

