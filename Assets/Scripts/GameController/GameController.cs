﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Prefab(nameof(GameController), true)]
public class GameController : Singleton<GameController> {

    internal PlayerShip selectedPlayerShip;

    internal Level SelectedLevel;


    public List<Enemy> enemies;

    public List<SpaceGarbage> garbage;

    private bool vibration = false;

    private PersistentData persistentData;

    [SerializeField]
    private GUILevel levelUIInfo;

    [SerializeField]
    private GUIPlayerShip playerShipUIInfo;

    public PersistentData PersistentData { get => persistentData; }
    public GUILevel LevelUIInfo { get => levelUIInfo; }
    public GUIPlayerShip PlayerShipUIInfo { get => playerShipUIInfo; }
    public bool Vibration { get => vibration; set => vibration = value; }

    internal void PauseGame(bool value)
    {
        Time.timeScale = value ? 0 : 1;
    }

    public void SaveInformation()
    {
        string data = JsonUtility.ToJson(PersistentData);
        PlayerPrefs.SetString("UserData", data);
    }

    public void ReadInformation()
    {
        string data = PlayerPrefs.GetString("UserData");
        JsonUtility.FromJsonOverwrite(data, PersistentData);
    }

    public void Initialize()
    {
        if (PersistentData == null)
        {
            persistentData = new PersistentData();
        }
        ReadInformation();

    }
    public List<Level> GetLevelsUnlocked()
    {
        List<Level> levelsUnlocked = new List<Level>();
        foreach (int item in PersistentData.LevelsUnlocked)
        {
            Level levelUnlocked = LevelUIInfo.GetLevelById(item);
            if (levelUnlocked)
            {
                levelsUnlocked.Add(levelUnlocked);
            }
        }
        return levelsUnlocked;
    }
}
