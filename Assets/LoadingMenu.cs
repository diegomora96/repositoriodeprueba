﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadingMenu : MonoBehaviour {
    // Start is called before the first frame update
    [SerializeField]
    private GameObject buttonContinue;

    [SerializeField]
    private TextMeshProUGUI textLoading;

    private GameController gameController;
    private Animator animator;
    private CanvasGroup canvasGroup;
    private AsyncOperation asyncLoad;

    void Start()
    {
        gameController = GameController.Instance;
        animator = GetComponent<Animator>();
        canvasGroup = GetComponent<CanvasGroup>();
        buttonContinue.gameObject.SetActive(false);
        if (!gameController.SelectedLevel)
        {

            gameController.SelectedLevel = gameController.LevelUIInfo.levels[0];
        }
 
        StartCoroutine(LoadYourAsyncScene());
    }

    // Update is called once per frame
    void Update()
    {

    }




    public void LoadScene()
    {
        //Debug.Log("Completed");
        //soundManager.PlayMusicClickStartGame();
        asyncLoad.allowSceneActivation = true;
    }

    IEnumerator LoadYourAsyncScene()
    {
        yield return new WaitForSeconds(3.0f);

        asyncLoad = SceneManager.LoadSceneAsync(gameController.SelectedLevel.SceneName);

        asyncLoad.allowSceneActivation = false;

        while (!asyncLoad.isDone)
        {
            float progress = Mathf.Clamp01(asyncLoad.progress / 0.9f);
            //textProgress.text = progress * 100f + "%";
            Debug.Log(progress);
            if (asyncLoad.progress >= 0.9f)
            {
                buttonContinue.SetActive(true);
                textLoading.gameObject.SetActive(false);
                //coin.gameObject.SetActive(false);
                //textTitle.gameObject.SetActive(false);
            }
            //TODO pone Animacion
            textLoading.GetComponent<Animator>().SetBool("Shown", false);
            yield return new WaitForSeconds(0.8f);
            textLoading.GetComponent<Animator>().SetBool("Shown", true);
            yield return new WaitForSeconds(0.8f);
            yield return null;
        }
    }
}

//IEnumerator Spark()
//{

//    textLoading.gameObject.SetActive(false);
//    yield return new WaitForSeconds(0.1f);
//    textLoading.gameObject.SetActive(true);
//    yield return new WaitForSeconds(0.1f);

//    yield return null;
//}

